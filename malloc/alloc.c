/**
* Malloc Lab
* CS 241 - Fall 2018
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define ALIGN(size)    ((size + sizeof(double) - 1)/sizeof(double)) * sizeof(double)
#define SIZE(ptr)	(*((size_t*)ptr - 1) & ~1)
//(*((size_t*)p - 1) & ~1)
#define BLOCK_HEAD(ptr)		((size_t *)ptr - 1)
#define BLOCK_END(ptr)		((size_t *)((char *)ptr + SIZE(ptr)))
#define EXTRA_SPACE		2048L

typedef struct strcut_metadata_entry_t{
	struct strcut_metadata_entry_t* next;
	struct strcut_metadata_entry_t* prev;
} entry_t;

static entry_t *head = NULL;
static void* top;	//top of memory
static void* bottom;	//bottom of memory

static int initial = 0; 

/**
 * Allocate space for array in memory
 *
 * Allocates a block of memory for an array of num elements, each of them size
 * bytes long, and initializes all its bits to zero. The effective result is
 * the allocation of an zero-initialized memory block of (num * size) bytes.
 *
 * @param num
 *    Number of elements to be allocated.
 * @param size
 *    Size of elements.
 *
 * @return
 *    A pointer to the memory block allocated by the function.
 *
 *    The type of this pointer is always void*, which can be cast to the
 *    desired type of data pointer in order to be dereferenceable.
 *
 *    If the function failed to allocate the requested block of memory, a
 *    NULL pointer is returned.
 *
 * @see http://www.cplusplus.com/reference/clibrary/cstdlib/calloc/
 */
void *calloc(size_t num, size_t size) {
    // implement calloc!
    void * ptr = malloc(num * size);

    if(ptr == NULL) return ptr;

    size_t *block_head = (size_t *)ptr - 1;
    memset(ptr, 0, (*block_head & ~1));
    return ptr;
}

/**
 * Allocate memory block
 *
 * Allocates a block of size bytes of memory, returning a pointer to the
 * beginning of the block.  The content of the newly allocated block of
 * memory is not initialized, remaining with indeterminate values.
 *
 * @param size
 *    Size of the memory block, in bytes.
 *
 * @return
 *    On success, a pointer to the memory block allocated by the function.
 *
 *    The type of this pointer is always void*, which can be cast to the
 *    desired type of data pointer in order to be dereferenceable.
 *
 *    If the function failed to allocate the requested block of memory,
 *    a null pointer is returned.
 *
 * @see http://www.cplusplus.com/reference/clibrary/cstdlib/malloc/
 */

void remove_block(entry_t *ptr){
	if(ptr == NULL) return;

	entry_t *curr = ptr;

	//if curr is the head of list and is the only block, just set head to null
	if(curr->next == NULL && curr->prev == NULL){
		head = NULL;
		return;
	}

	//curr is the head of list
	if(curr -> prev == NULL){
		head = head -> next;
		curr -> next -> prev = NULL;
		curr -> next = NULL;
		return;
	}
	//curr is the tail of list
	if(curr -> next == NULL){
		curr -> prev -> next = NULL;
		curr -> prev = NULL;
		return;
	}

	//if curr in the middle of list
	curr -> prev -> next = curr -> next;
	curr -> next -> prev = curr -> prev;
	curr -> next = NULL;
	curr -> prev = NULL;
	return;
}

void add_to_list(entry_t *ptr){
	//add to the head block
	if(ptr == NULL) return;

	entry_t *curr = ptr;

	if(head == NULL){
		head = curr;
		head -> next = NULL;
		head -> prev = NULL;
		return;
	}

	curr -> next = head;
	curr -> prev = NULL;
	curr -> next -> prev = curr;
	head = curr;
	return;
}

//only see the next block since we just use this function after split.
void merge_block(entry_t *ptr){
	size_t *block_head = BLOCK_HEAD(ptr);
	size_t *tail = BLOCK_END(ptr);

	//if the block is not the top of heap, try to merge with the next block
	if(tail + 1 != top){
		size_t * next_block_head = tail + 1;
		if((*(next_block_head) & 1) == 0){
			entry_t *another_new_block = (entry_t *)(next_block_head + 1);
			size_t *new_block_end = BLOCK_END(another_new_block);

			size_t new_size = ((*tail) & ~1) + 2 * sizeof(size_t) + ((*next_block_head) & ~1);
			remove_block(another_new_block);

			*new_block_end = new_size;
			*block_head = new_size;
		}

	}
}


void *alloc_with_block(entry_t * ptr, size_t size){
	if(SIZE(ptr) > size + 2*sizeof(size_t) + sizeof(entry_t)){
		size_t new_size = SIZE(ptr) - size - 2*sizeof(size_t);

		size_t *block_head = BLOCK_HEAD(ptr);
		size_t *tail = BLOCK_END(ptr);	//tail of orginal block, after split will be the tail of splited block

		size_t *new_tail = (size_t *)((char *)ptr + size); // new tail of original block
		size_t *new_head = new_tail + 1;

		entry_t *new_entry_t = (entry_t *)(new_head + 1);

		//remove the front block
		remove_block(ptr);

		//set the front block to be in used
		*block_head = size + 1;
		*new_tail = size + 1;

		//set the size of splited block
		*new_head = new_size;
		*tail = new_size;

		//add the splited block to the free list and maybe will merge with the next block
		add_to_list(new_entry_t);
		//new_entry_t = (entry_t *)merge_block(new_entry_t);

		merge_block(new_entry_t);
	}else{
		//remove the front block
		size_t *block_head = BLOCK_HEAD(ptr);
		size_t *tail = BLOCK_END(ptr);

		*block_head += 1;
		*tail += 1;

		remove_block(ptr);
	}

	return ptr;
}


void *malloc(size_t size) {
    // implement malloc!
    entry_t *p = head;
    size = ALIGN(size); //Alignment and rounding up with 16

    if(size < sizeof(entry_t))
    	size = sizeof(entry_t);

    while(p != NULL){
    	if(SIZE(p) >= size ){
    		break;
    	}
    	p = p -> next;
    }

    if(p){
    	return alloc_with_block(p, size);
    }


    //size_t total = size + 2 * sizeof(size_t) + EXTRA_SPACE; //by using extra space to avoid more call of sbrk.

    size_t total = size + 2 * sizeof(size_t);

    size_t* block_head = (size_t*)sbrk(total); //sbrk return original position not the top of heap!
    
    if (initial == 0){
        initial = 1;
        bottom = (void*) block_head;
    }
    top = (void *)((char*)block_head + total);

    size_t *block_end = (size_t *)top - 1;

    *block_head = size + 1;
    *block_end = size + 1;

    return (void *) (block_head + 1);

}

/**
 * Deallocate space in memory
 *
 * A block of memory previously allocated using a call to malloc(),
 * calloc() or realloc() is deallocated, making it available again for
 * further allocations.
 *
 * Notice that this function leaves the value of ptr unchanged, hence
 * it still points to the same (now invalid) location, and not to the
 * null pointer.
 *
 * @param ptr
 *    Pointer to a memory block previously allocated with malloc(),
 *    calloc() or realloc() to be deallocated.  If a null pointer is
 *    passed as argument, no action occurs.
 */

void free(void *ptr) {
    // implement free!
    if(!ptr) return;

    size_t *block_head = BLOCK_HEAD(ptr);
    size_t *block_end = BLOCK_END(ptr);

    (*block_end) = *block_end & ~1;
    (*block_head) = *block_head & ~1;
    entry_t* curr = (entry_t*)ptr;

    //this is the only block
    if(block_end + 1 == top && block_head == bottom){
    	add_to_list(curr);
    	return;
    }
    if(top == block_end + 1){
    	//if prev block is free, then merge with it and return or add this block to the list
    	size_t *prev_block_end = block_head - 1;
    	size_t *prev_block_head = (size_t *)((char *)prev_block_end - ((*prev_block_end) & ~1)) - 1;
    	if(((*prev_block_end) & 1) == 0){
    		size_t new_size = ((*prev_block_end) & ~1) + ((*block_head) & ~1) + 2*sizeof(size_t);

    		*prev_block_head = new_size;
    		*block_end = new_size;
    		return;
    	}
    	add_to_list(curr);
    	return;
    }

    if(bottom == block_head){
    	size_t *next_block_head = block_end + 1;
    	size_t *next_block_end = (size_t *)((char *)(next_block_head + 1) + ((*next_block_head) & ~1));
    	if(((*next_block_head) & 1) == 0){
    		size_t new_size = ((*block_head) & ~1) + ((*next_block_head) & ~1) + 2*sizeof(size_t);
    		*(next_block_end) = new_size;
    		*(block_head) = new_size;
    		entry_t * next_block = (entry_t *)(next_block_head + 1);
    		remove_block(next_block);
    		add_to_list(curr);
    		return;
    	}
    	add_to_list(curr);
    	return;
    }

    //if curr block in the middle of list
    size_t *next_block_head = block_end + 1;
	size_t *prev_block_end = block_head - 1;
	size_t *next_block_end = (size_t *)((char *)(next_block_head + 1) + ((*next_block_head) & ~1));
	size_t *prev_block_head = (size_t *)((char *)prev_block_end - ((*prev_block_end) & ~1)) - 1;
	entry_t *next_block = (entry_t *)(next_block_head + 1);

	if(((*next_block_head) & 1) != 0 && ((*prev_block_end) & 1) != 0){
		add_to_list(curr);
		return;
	}

	if(((*next_block_head) & 1) == 0 && ((*prev_block_end) & 1) != 0){
		size_t new_size = ((*next_block_head) & ~1) + ((*block_head) & ~1) + 2*sizeof(size_t);
		*block_head = new_size;
		*next_block_end = new_size;
		remove_block(next_block);
		add_to_list(curr);
		return;
	}

	if(((*next_block_head) & 1) != 0 && ((*prev_block_end) & 1) == 0){
		size_t new_size = ((*prev_block_head) & ~1) + ((*block_head) & ~1) + 2*sizeof(size_t);
		*prev_block_head = new_size;
		*block_end = new_size;
		return;
	}

	if(((*next_block_head) & 1) == 0 && ((*prev_block_end) & 1) == 0){
		size_t new_size = ((*next_block_head) & ~1) + ((*prev_block_head) & ~1) + ((*block_head) & ~1) + 4*sizeof(size_t);
		*prev_block_head = new_size;
		*next_block_end = new_size;
		remove_block(next_block);
		return;
	}
    return;
}


/**
 * Reallocate memory block
 *
 * The size of the memory block pointed to by the ptr parameter is changed
 * to the size bytes, expanding or reducing the amount of memory available
 * in the block.
 *
 * The function may move the memory block to a new location, in which case
 * the new location is returned. The content of the memory block is preserved
 * up to the lesser of the new and old sizes, even if the block is moved. If
 * the new size is larger, the value of the newly allocated portion is
 * indeterminate.
 *
 * In case that ptr is NULL, the function behaves exactly as malloc, assigning
 * a new block of size bytes and returning a pointer to the beginning of it.
 *
 * In case that the size is 0, the memory previously allocated in ptr is
 * deallocated as if a call to free was made, and a NULL pointer is returned.
 *
 * @param ptr
 *    Pointer to a memory block previously allocated with malloc(), calloc()
 *    or realloc() to be reallocated.
 *
 *    If this is NULL, a new block is allocated and a pointer to it is
 *    returned by the function.
 *
 * @param size
 *    New size for the memory block, in bytes.
 *
 *    If it is 0 and ptr points to an existing block of memory, the memory
 *    block pointed by ptr is deallocated and a NULL pointer is returned.
 *
 * @return
 *    A pointer to the reallocated memory block, which may be either the
 *    same as the ptr argument or a new location.
 *
 *    The type of this pointer is void*, which can be cast to the desired
 *    type of data pointer in order to be dereferenceable.
 *
 *    If the function failed to allocate the requested block of memory,
 *    a NULL pointer is returned, and the memory block pointed to by
 *    argument ptr is left unchanged.
 *
 * @see http://www.cplusplus.com/reference/clibrary/cstdlib/realloc/
 */
void *realloc(void *ptr, size_t size) {
    // implement realloc!
	if(!ptr) return malloc(size);

	size_t old_size = SIZE(ptr);
	//size_t new_size = ALIGN(size);
	if(size <= old_size) return ptr;


	size_t *block_head = BLOCK_HEAD(ptr);
    size_t *block_end = BLOCK_END(ptr);

    if(block_end + 1 == top && block_head == bottom){
    	void *output = malloc(size);
    	memcpy(output, ptr, old_size);
		free(ptr);
    	return output;
    }

    if(top == block_end + 1){
    	size_t *prev_block_end = block_head - 1;
    	size_t *prev_block_head = (size_t *)((char *)prev_block_end - ((*prev_block_end) & ~1)) - 1;
    	size_t new_size = ((*prev_block_end) & ~1) +  ((*block_end) & ~1) + 2*sizeof(size_t);

    	if(((*prev_block_end) & 1) == 0 && new_size  > size){
    		*prev_block_head = new_size;
    		*block_end = new_size;
    		remove_block((entry_t*)(prev_block_head + 1));

    		memmove(prev_block_head + 1, ptr, old_size);

    		*prev_block_head += 1;
    		*block_end += 1;

    		return (void *)(prev_block_head + 1);

    	}else{
    		void *output = malloc(size);
    		memcpy(output, ptr, old_size);
			free(ptr);
    		return output;
    	}
    }

    if(bottom == block_head){
		size_t *next_block_head = block_end + 1;
    	size_t *next_block_end = (size_t *)((char *)(next_block_head + 1) + ((*next_block_head) & ~1));
    	size_t new_size = ((*next_block_end) & ~1) +  ((*block_end) & ~1) + 2*sizeof(size_t);

    	if(((*next_block_end) & 1) == 0 && new_size  > size){
    		*block_head = new_size + 1;
    		*next_block_end = new_size + 1;
    		remove_block((entry_t*)(next_block_head + 1));
    		return (void *)(block_head + 1);

    	}else{
    		void *output = malloc(size);
    		memcpy(output, ptr, old_size);
			free(ptr);
    		return output;
    	}


    }


    size_t *next_block_head = block_end + 1;
	size_t *prev_block_end = block_head - 1;
	size_t *next_block_end = (size_t *)((char *)(next_block_head + 1) + ((*next_block_head) & ~1));
	size_t *prev_block_head = (size_t *)((char *)prev_block_end - ((*prev_block_end) & ~1)) - 1;

	if(((*next_block_head) & 1) != 0 && ((*prev_block_end) & 1) != 0){
		void *output = malloc(size);
    	memcpy(output, ptr, old_size);
		free(ptr);
    	return output;
	}

	if(((*next_block_head) & 1) == 0 && ((*prev_block_end) & 1) != 0){
		size_t new_size = ((*next_block_end) & ~1) +  ((*block_end) & ~1) + 2*sizeof(size_t);
		if(new_size > size){
			*block_head = new_size + 1;
    		*next_block_end = new_size + 1;
    		remove_block((entry_t*)(next_block_head + 1));
    		return (void *)(block_head + 1);
		}else{
			void *output = malloc(size);
    		memcpy(output, ptr, old_size);
			free(ptr);
    		return output;
		}
		
	}

	if(((*next_block_head) & 1) != 0 && ((*prev_block_end) & 1) == 0){
		size_t new_size = ((*prev_block_end) & ~1) +  ((*block_end) & ~1) + 2*sizeof(size_t);

		if(new_size > size){
			*prev_block_head = new_size;
    		*block_end = new_size;

    		remove_block((entry_t*)(prev_block_head + 1));
    		memmove(prev_block_head + 1, ptr, old_size);

    		*prev_block_head += 1;
    		*block_end += 1;

    		return (void *)(prev_block_head + 1);
		}else{
			void *output = malloc(size);
    		memcpy(output, ptr, old_size);
			free(ptr);
    		return output;
		}
	}

	if(((*next_block_head) & 1) == 0 && ((*prev_block_end) & 1) == 0){
		size_t new_size = ((*next_block_head) & ~1) + ((*prev_block_head) & ~1) + ((*block_head) & ~1) + 4*sizeof(size_t);

		if(new_size > size){
			*prev_block_head = new_size;
    		*next_block_end = new_size;

    		remove_block((entry_t*)(prev_block_head + 1));
    		remove_block((entry_t*)(next_block_head + 1));

    		memmove(prev_block_head + 1, ptr, old_size);

    		*prev_block_head += 1;
    		*next_block_end += 1;

    		return (void *)(prev_block_head + 1);
		}else{
			void *output = malloc(size);
    		memcpy(output, ptr, old_size);
			free(ptr);
    		return output;
		}
	}

	void *output = malloc(size);
	memcpy(output, ptr, old_size);
	free(ptr);
    return output;
}
